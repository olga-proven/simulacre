import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import os
from urllib.request import urlretrieve


# Contingut original.
filepath: str = "incidence-rate-2021-raw.csv"

# Et donem el contingut ja preprocessat, a partir de la q3.
filepath_q3: str = "incidence-rate-2021-new.csv"

def q0_read_raw_data():
    df_orig = pd.read_csv(filepath, sep=';', decimal=".")
    print(df_orig.iloc[0])
    return df_orig





def q0_read_correct_data():
    df_orig = pd.read_csv(filepath_q3, sep=';', decimal=".")
    # print(df_orig.iloc[0])
    return df_orig




# Pregunta 1. Mostra la següent info: 
#               El número de files en total 
#               Informació sobre totes les columnes
#               Les 20 primeres files 
def q1_basic_info(df: pd.DataFrame):

    print(df.iloc[0])

    numero_de_files = df.shape[0]
    print("El número de files en total:", numero_de_files)


    info = df.info()
    #print("Informació sobre totes les columnes: ", info)

    top20= df.head(20)
    print("Les 20 primeres files: ", top20) 

# Pregunta 2. Preprocessament dataframe.
#               Mostra els valors únics del camp DENOMINATOR
#               Crea un nou dataframe que:
#                 Elimina la columna DISEASE_DESCRIPTION
#                 De la columna 'GROUP' només volem les files 
#                   que tinguin el valor 'COUNTRIES'
#                 De la columna 'DENOMINATOR' Només tingui el valor
#                   'per 1,000,000 total population'.
#                   per tal d'unificar criteris.
#               Guarda-ho en un fitxer anomenat:
#                  incidence-rate-2021-new.csv
def q2_preprocessing(df: pd.DataFrame):
    print(df["DENOMINATOR"].unique())
    df_drop =  df.drop(["DISEASE_DESCRIPTION"], axis=1)
    df_new = df_drop.loc[(df["GROUP"] == "COUNTRIES") & (df_drop["DENOMINATOR"] == "per 1,000,000 total population")]
    #print(df_new.info())
    df_new.to_csv('dataset.csv', index=False)
    return df_new

# Pregunta 3. Analitzem NaN i outliers.
#               Mostra els valors NaN que hi ha en cada columna.
#               Mostra el nom, enfermetat, inc_rate i any de les 10 files que tenen valors més
#               alts d'INCIDENCE_RATE.
def q3_outliers(df_new: pd.DataFrame):
    filas_with_na = df_new.isna().any(axis=1)
    # print the rows with NA values
    print("Filas with NaN values",df_new[filas_with_na])

    incidents = df_new.sort_values(by="INCIDENCE_RATE", ascending=False).head(10)
    columnas = incidents[["NAME", 
         "DISEASE",
         "YEAR",
         "INCIDENCE_RATE"]]
    print(columnas)


    return df_new

# Pregunta 4. Gràfic distribució casos enfermetats.
#               Mostra un gràfic plotbox o similar dels casos d'enfermetats d'un país
#               (el que et toqui) agrupats per enfermetats.
def q4_grafic_distribucio(df_new: pd.DataFrame):
    zimbabwe =  df_new[df_new['NAME'] == "Zimbabwe"]
    print(zimbabwe.head(10))
    zimbabwe_casos =  zimbabwe[[
         "DISEASE",
          "INCIDENCE_RATE"]]
    print(zimbabwe_casos.head(10))
    casos = zimbabwe_casos.groupby(["DISEASE"]).sum().reset_index()
    print(casos)

    #sns.barplot(x='DISEASE', y='INCIDENCE_RATE', data=casos)
    plt.figure(figsize=(18, 8))  # the size of the figure
    sns.set(style="ticks")  # the style
    graph = sns.barplot(data=casos, x='DISEASE', y='INCIDENCE_RATE')
    
    # Put labels and title
    graph.set(xlabel="Disease", ylabel="Number of cases")

    graph.set_xlabel("Disease", fontsize=12)  # Font size for x-label
    graph.set_ylabel("Number of cases", fontsize=12)  # Font size for y-label
    #graph.title("Diseases in Zimbabwe")
    plt.savefig('graph_1.png')
    plt.show()
    plt.close()

# Pregunta 6. Mostra 2 gràfics a la pàgina web
#               Un on es vegi la incidència de les enfermetats d'un país(el que tu vulguis) 
#               des del 1980 fins el 2021, ambdós inclosos.
"""     afghanistan =  df_new[df_new['NAME'] == "Afghanistan"]
    print(afghanistan.head(10))
   
    afghanistan_casos_per_year = afghanistan[(afghanistan['YEAR'] >= 1980) & (afghanistan['YEAR'] <= 2021)]
    print(afghanistan_casos_per_year.head(10))
    afghanistan_year_disease_rate=afghanistan_casos_per_year[[
         "DISEASE",
          "INCIDENCE_RATE","YEAR"]].reset_index()
    afghanistan_year_disease_rate= afghanistan_year_disease_rate.dropna()
    print(afghanistan_year_disease_rate.head(10))

    sns.set(style="whitegrid")
    plt.figure(figsize=(24, 6))
    sns.barplot(data=afghanistan_year_disease_rate, x="YEAR", y="INCIDENCE_RATE", hue="DISEASE")
    plt.title("Deseases in Afganistan from 1980 until 2021")
    plt.xlabel("Year")
    plt.ylabel("Incidence rate")
    plt.savefig('graph_2.png')
    plt.show() """

"""     plot_result2 = __draw_plot(plt)
    plt.close()
    return plot_result1, plot_result2 """


    


if __name__ == "__main__":
    df = q0_read_raw_data()
    q1_basic_info(df)
    df_new = q2_preprocessing(df)
    df_new = pd.read_csv(filepath, sep=';', decimal=".")
    q3_outliers(df_new)
    q4_grafic_distribucio(df_new)